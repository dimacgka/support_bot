package main

import (
	"log"
	"newProject/project/config"
	"newProject/project/internal/connection"
)

func main() {
	v, err := config.LoadConfig()
	if err != nil {
		log.Fatal("Cannot load config: ", err.Error())
	}
	cfg, err := config.ParseConfig(v)
	if err != nil {
		log.Fatal("Config parse error", err.Error())
	}
	log.Println("Config loaded")
	bot, err := connection.NewTelegramBot(cfg)
	if err != nil {
		log.Fatal("Bot not loaded", err.Error())
	}

	if err = bot.Run(); err != nil {
		log.Fatal("Cannot to launch bot", err.Error())
	}
	log.Println("Bot is working...")
}
