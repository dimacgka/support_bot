package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	System         SystemConfig
	Bot            BotConfig
	Channel        Channel
	PostgresConfig PostgresConfig
}

type PostgresConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"-"`
	DBName   string `json:"DBName"`
	SSLMode  string `json:"sslMode"`
	PgDriver string `json:"pgDriver"`
}

type SystemConfig struct {
	MaxGoRoutines int64
}

type BotConfig struct {
	Token      string
	AppVersion string
	Attempt    int64
}

type Channel struct {
	ChannelID int64
}

func LoadConfig() (*viper.Viper, error) {
	v := viper.New()
	v.AddConfigPath("config")
	v.SetConfigName("config")
	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func ParseConfig(v *viper.Viper) (*Config, error) {
	var c Config
	err := v.Unmarshal(&c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}
