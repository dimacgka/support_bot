package bids

const (
	CreateBid      = "Заполнить заявку на подключение"
	FAQ            = "FAQ"
	ContactSupport = "Связаться с оператором"
	Greetings      = "Доброго времени суток!"
	MonthStatistic = "/month_statistic"
	AreYouNotAdmin = "Это команда только для администраторов!"

	CreateBidText      = "Заполните данные по заявке, по пример"
	FAQText            = "Информация о нас!"
	ContactSupportText = "В течение некоторого времени наш оператор свяжется с Вами"

	AlreadyPressed = "Вы уже 3 раза нажали на кнопку"
)
