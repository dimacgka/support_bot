package bids

import (
	"fmt"
	tele "gopkg.in/telebot.v3"
	"log"
	"support_bot/config"
	"time"
)

type BidHandler struct {
	BidGoRepo BidGoRepo
	kernel    *tele.Bot
	cfg       config.Config
}

func NewBidHandler(BidGoRepo BidGoRepo, kernel *tele.Bot, cfg config.Config) BidHandler {
	return BidHandler{
		BidGoRepo: BidGoRepo,
		kernel:    kernel,
		cfg:       cfg,
	}
}

func getMessageStatistic(response GetUserStatisticResponse) string {
	message := "Количество пользователей: " + fmt.Sprintf("%d", response.AllIDs) + "\n" +
		"Количество новых пользователей за месяц: " + fmt.Sprintf("%d", response.MonthIDs) + "\n" +
		"Количество заявок на подключение за все время: " + fmt.Sprintf("%d", response.AllBids) + "\n" +
		"Количество заявок на подключение за месяц: " + fmt.Sprintf("%d", response.MonthBids) + "\n" +
		"Количество премиум аккаунтов: " + fmt.Sprintf("%d", response.PremiumAccounts) + "\n" +
		"Количество администраторов: " + fmt.Sprintf("%d", response.Admins) + "\n" +
		"Количество нажатий на кнопку подать заявку на подключение: " + fmt.Sprintf("%d", response.CountButtonPressed) + "\n"
	return message
}

func (b BidHandler) Attach() {
	b.kernel.Handle("/start", func(c tele.Context) error {
		btn1 := tele.ReplyButton{Text: CreateBid}
		btn2 := tele.ReplyButton{Text: FAQ}
		btn3 := tele.ReplyButton{Text: ContactSupport}

		keys := [][]tele.ReplyButton{
			{btn1},
			{btn2, btn3},
		}
		replyKeyboard := tele.ReplyMarkup{
			ReplyKeyboard:  keys,
			ResizeKeyboard: true,
		}
		err := c.Send(Greetings, &replyKeyboard)
		if err != nil {
			log.Fatal("Error in sending message!")
			return err
		}
		return nil
	})

	b.kernel.Handle(CreateBid, func(c tele.Context) error {
		userID := c.Sender().ID
		attempt, err := b.BidGoRepo.CheckUser(&User{
			ID:               c.Sender().ID,
			Name:             c.Sender().FirstName + c.Sender().LastName,
			Nickname:         c.Sender().Username,
			isPremiumAccount: c.Sender().IsPremium,
		})
		if err != nil {
			log.Fatal(err)
			return err
		}
		if int64(*attempt) > b.cfg.Bot.Attempt {
			c.Send(AlreadyPressed)
		} else {
			err := c.Send(CreateBidText)
			if err != nil {
				log.Fatal(err)
				return err
			}
			b.kernel.Handle(tele.OnText, func(c tele.Context) error {
				if err != nil {
					log.Fatal(err)
					return err
				}
				newBid := c.Message().Text
				username := c.Sender().Username
				message := "Новая заявка от " + username + ":\n" + newBid
				err = b.BidGoRepo.IncreaseAttempt(&userID)
				_, err = b.kernel.Send(&tele.Chat{ID: b.cfg.Channel.ChannelID}, message)
				if err != nil {
					log.Fatal(err)
					return err
				}
				return nil
			})
			return nil
		}
		return nil
	})

	b.kernel.Handle(FAQ, func(c tele.Context) error {
		err := c.Send(FAQText)
		if err != nil {
			log.Fatal(err)
			return err
		}
		return nil
	})

	b.kernel.Handle(ContactSupport, func(c tele.Context) error {
		username := c.Sender().Username
		err := c.Send(ContactSupportText)
		if err != nil {
			log.Fatal(err)
			return err
		}
		message := "Пользователь " + username + " хочет связаться с оператором!\n"
		_, err = b.kernel.Send(&tele.Chat{ID: b.cfg.Channel.ChannelID}, message)
		if err != nil {
			log.Fatal(err)
			return err
		}
		return nil
	})

	b.kernel.Handle(MonthStatistic, func(c tele.Context) error {
		userID := c.Sender().ID
		isAdmin, err := b.BidGoRepo.IsAdmin(&userID)
		if err != nil {
			log.Fatal(err)
			return err
		}
		if *isAdmin == false {
			err = c.Send(AreYouNotAdmin)
			if err != nil {
				log.Fatal(err)
				return err
			}
		} else {
			now := time.Now().UTC()
			activeMonth := now.Month()
			start := time.Date(now.Year(), activeMonth, 1, 0, 0, 0, 0, time.UTC)
			response, err := b.BidGoRepo.GetUserStatistic(start, now)
			if err != nil {
				log.Fatal(err)
				return err
			}
			message := getMessageStatistic(*response)
			err = c.Send(message)
			if err != nil {
				log.Fatal(err)
				return err
			}
		}
		return nil
	})
}
