package bids

type User struct {
	ID               int64  `db:"id"`
	Name             string `db:"user_name"`
	Nickname         string `db:"nickname"`
	isPremiumAccount bool   `db:"is_premium"`
}

type GetUserStatisticResponse struct {
	AllIDs             int64 `db:"count_all_id"`
	MonthIDs           int64 `db:"count_month_id"`
	CountButtonPressed int64 `db:"count_button_press"`
	AllBids            int64 `db:"count_all_bids"`
	MonthBids          int64 `db:"count_month_bids"`
	PremiumAccounts    int64 `db:"count_premium"`
	Admins             int64 `db:"count_admin"`
}
