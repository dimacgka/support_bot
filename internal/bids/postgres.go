package bids

import (
	_ "fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"time"
)

type BidGoRepo struct {
	kernel *sqlx.DB
}

func NewBidGoRepo(kernel *sqlx.DB) BidGoRepo {
	return BidGoRepo{kernel: kernel}
}

func (b BidGoRepo) CheckUser(params *User) (*int, error) {
	var attempt int
	query :=
		`WITH check_user AS (
    SELECT * FROM users WHERE id = $1
)
INSERT INTO users (id, user_name, is_premium, nickname, date_first_entry, is_sending_bid)
SELECT $1, $2, $3, $4, NOW(), false
WHERE NOT EXISTS (SELECT * FROM check_user);`
	_, err := b.kernel.Exec(query, params.ID, params.Name,
		params.isPremiumAccount, params.Nickname)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	query2 := `
SELECT attempt FROM users WHERE id = $1`
	err = b.kernel.Select(&attempt, query2, params.ID)
	return &attempt, nil
}

func (b BidGoRepo) IncreaseAttempt(userID *int64) error {
	query :=
		`UPDATE users
SET attempt = attempt+1,
    is_sending_bid = true
WHERE id =$1;`
	_, err := b.kernel.Exec(query, userID)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func (b BidGoRepo) IsAdmin(userID *int64) (*bool, error) {
	var exists bool
	err := b.kernel.QueryRow("SELECT COALESCE((SELECT is_admin FROM users WHERE id=$1), false)", userID).Scan(&exists)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return &exists, nil
}

func (b BidGoRepo) GetUserStatistic(start time.Time, end time.Time) (*GetUserStatisticResponse, error) {
	var result []GetUserStatisticResponse
	query := `
SELECT COALESCE(COUNT(id), 0) AS count_all_id,
       COALESCE(COUNT((SELECT id WHERE date_first_entry::DATE BETWEEN $1::DATE AND $2::DATE)), 0) count_month_id,
       SUM(attempt) AS count_button_press,
       COALESCE(COUNT((SELECT id WHERE is_sending_bid=true)), 0) AS count_all_bids,
       COALESCE(COUNT((SELECT id WHERE date_first_entry::DATE BETWEEN $1::DATE AND $2::DATE AND is_sending_bid=true)), 0) AS count_month_bids,
       COALESCE(COUNT((SELECT id WHERE is_premium=true)), 0) AS count_premium,
       COALESCE(COUNT((SELECT id WHERE is_admin=true)), 0) as count_admin
FROM users`
	err := b.kernel.Select(&result, query, start, end)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return &result[0], nil
}
