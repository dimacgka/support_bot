package connection

import (
	tele "gopkg.in/telebot.v3"
	"support_bot/config"
	"time"
)

type TelegramBot struct {
	core *tele.Bot
	cfg  *config.Config
}

func NewTelegramBot(cfg *config.Config) (*TelegramBot, error) {
	settings := tele.Settings{
		Token:  cfg.Bot.Token,
		Poller: &tele.LongPoller{Timeout: 10 * time.Second},
	}
	core, err := tele.NewBot(settings)
	if err != nil {
		return nil, err
	}
	return &TelegramBot{
		core: core,
		cfg:  cfg,
	}, nil
}

func (t TelegramBot) Run() error {
	if err := t.MapHandlers(t.cfg); err != nil {
		return err
	}
	t.core.Start()
	return nil
}

func (t TelegramBot) Shutdown() {
	t.core.Stop()
}
