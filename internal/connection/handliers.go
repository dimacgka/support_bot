package connection

import (
	"support_bot/config"
	"support_bot/internal/bids"
	"support_bot/pkg/storage"
)

func (t *TelegramBot) MapHandlers(cfg *config.Config) error {
	bidBD, err := storage.InitBidDb(cfg)
	if err != nil {
		return err
	}
	bidGoRepo := bids.NewBidGoRepo(bidBD)
	bidHandler := bids.NewBidHandler(bidGoRepo, t.core, *cfg)
	bidHandler.Attach()

	return nil
}
