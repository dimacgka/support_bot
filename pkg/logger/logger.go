package logger

//
//import (
//	"fmt"
//	"github.com/afiskon/promtail-client/promtail"
//	"log"
//	"newProject/project/config"
//	"time"
//)
//
//type StatsLogger struct {
//	cfg       *config.Config
//	lokiStats promtail.Client
//}
//
//func NewStatsLogger(cfg *config.Config) *StatsLogger {
//	return &StatsLogger{
//		cfg: cfg,
//	}
//}
//
//var conf = promtail.ClientConfig{
//	BatchWait:          5 * time.Second,
//	BatchEntriesNumber: 10000,
//	SendLevel:          promtail.INFO,
//	PrintLevel:         promtail.DISABLE,
//}
//var marginLabels = "{source=\"STAT_BOT\",level=\"MARGIN\",period=\"%s\",margin=\"%d\",month_margin=\"%d\",turnover_in=\"%d\",turnover_out=\"%d\",conversion_in=\"%d\",conversion_out=\"%d\",amount_tx_in=\"%d\",amount_tx_out=\"%d\"}"
//
//const (
//	TODAY = "daily"
//	MONTH = "month"
//	INIT  = "init"
//)
//
//type StatsData struct {
//	Margin        int64
//	MonthMargin   int64
//	TurnoverIn    int64
//	TurnoverOut   int64
//	ConversionIn  int64
//	ConversionOut int64
//	AmountTxIn    int64
//	AmountTxOut   int64
//}
//
//func (sl *StatsLogger) InitLogger() error {
//	var err error
//	conf.Labels = fmt.Sprintf(marginLabels, INIT, 0, 0, 0, 0, 0, 0, 0, 0)
//	//conf.PushURL = fmt.Sprintf("%s/api/prom/push", sl.cfg.URL)
//	sl.lokiStats, err = promtail.NewClientJson(conf)
//	if err != nil {
//		log.Fatalf(err.Error())
//	}
//
//	return nil
//}
//
//func (sl *StatsLogger) SendMonthData(params *StatsData) {
//	sl.lokiStats.Infof(marginLabels, MONTH,
//		params.Margin, params.MonthMargin, params.TurnoverIn, params.TurnoverOut,
//		params.ConversionIn, params.ConversionOut,
//		params.AmountTxIn, params.AmountTxOut,
//	)
//}
//
//func (sl *StatsLogger) SendDailyData(params *StatsData) {
//	log.Printf("%v", *params)
//	sl.lokiStats.Infof(marginLabels, TODAY,
//		params.Margin, params.MonthMargin,
//		params.TurnoverIn, params.TurnoverOut,
//		params.ConversionIn, params.ConversionOut,
//		params.AmountTxIn, params.AmountTxOut,
//	)
//}
