package storage

import (
	"fmt"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"log"
	"support_bot/config"
)

func InitBidDb(c *config.Config) (*sqlx.DB, error) {
	connectionUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		c.PostgresConfig.Host,
		c.PostgresConfig.Port,
		c.PostgresConfig.User,
		c.PostgresConfig.Password,
		c.PostgresConfig.DBName,
		c.PostgresConfig.SSLMode)
	database, err := sqlx.Connect(c.PostgresConfig.PgDriver, connectionUrl)
	fmt.Println(connectionUrl)
	if err != nil {
		return nil, err
	}
	log.Println("Connected to DataBase")
	database.SetMaxOpenConns(50)
	return database, nil
}
